const getRewards = () => {
    return [
        {
            id: 1,
            name: "Espresso",
            limit: 0.1,
            image: "/assets/images/rewards/espresso.jpg",
            earned: true
        },
        {
            id: 2,
            name: "Capuccino",
            limit: 0.18,
            image: "/assets/images/rewards/capuccino.jpeg",
            earned: true
        },
        {
            id: 3,
            name: "Cup of Tea",
            image: "/assets/images/rewards/tea.gif",
            limit: 0.25,
        },
        {
            id: 4,
            name: "Glass of Beer",
            limit: 0.5,
            image: "/assets/images/rewards/beer.jpeg",
        },
        {
            id: 5,
            name: "Bottle of Wine",
            limit: 0.75,
            image: "/assets/images/rewards/wine.jpeg",
        },
        {
            id: 6,
            name: "Watering Can",
            limit: 1.5,
            image: "/assets/images/rewards/kanne.jpg",
        },
        {
            id: 7,
            name: "Barrel",
            limit: 220,
            image: "/assets/images/rewards/barrel.jpeg",
        },
        {
            id: 8,
            name: "Jacuzzi",
            limit: 1500,
            image: "/assets/images/rewards/jacuzzi.jpeg",
        },
        {
            id: 9,
            name: "Fire Truck",
            limit: 3785,
            image: "/assets/images/rewards/truck.jpeg",
        },
        {
            id: 10,
            name: "Olympic Pool",
            limit: 2500000,
            image: "/assets/images/rewards/pool.jpg",
        },
        {
            id: 11,
            name: "Trevi Fountain",
            limit: 79989428,
            image: "/assets/images/rewards/fountain.jpeg",
        },
    ];
}

module.exports = {
    getRewards
}
