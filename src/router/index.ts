import { createRouter, createWebHistory } from "@ionic/vue-router";
import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
    {
        path: "",
        redirect: "/login",
    },
    {
        path: "/login",
        name: "Login",
        component: () => import("../views/Login.vue"),
    },
    {
        path: "/profile",
        name: "Profile",
        component: () => import("../views/Profile.vue"),
    },
    {
        path: "/dashboard",
        name: "Dashboard",
        component: () => import("../views/Dashboard.vue"),
    },
    {
        path: "/session",
        name: "Session",
        component: () => import("../views/Session.vue"),
    },
    {
        path: "/history",
        name: "History",
        component: () => import("../views/History.vue"),
    },
    {
        path: "/connect",
        name: "Connect",
        component: () => import("../views/ConnectDevice.vue"),
    },
    {
        path: "/rewards",
        name: "Rewards",
        component: () => import("../views/Rewards.vue"),
    },
    {
        path: "/trends",
        name: "Trends",
        component: () => import("../views/Trends.vue"),
    },
    {
        path: "/device",
        name: "Device",
        component: () => import("../views/Device.vue"),
    }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

export default router;
