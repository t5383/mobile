# Tubeo Mobile App

## Installation

1. Install NPM dependencies with:

    ```bash
    $ npm install
    ```

2. Run application locally:

   ```bash
   $ ionic serve
   ```
   
    or run on your iPhone through:

   ```bash
   $ ionic capacitor run ios -l --external
   ```
   
   or run on your Android phone through:
   
   ```bash
   $ ionic capacitor run android -l --external
   ```
